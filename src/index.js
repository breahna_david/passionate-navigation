import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Navigation from './components/navigation/navigation.jsx';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Navigation />, document.getElementById('root'));
registerServiceWorker();
