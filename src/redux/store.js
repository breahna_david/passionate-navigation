import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import navigation from './navigation';

const reducers = combineReducers({
  navigation
});

// Define redux middlewares
const middlewares = [thunk];

// Log if we are in development mode
if (process.env.NODE_ENV === 'development') {
  middlewares.push(createLogger({ timestamp: false }));
}

// Make store with middlewares
const store = compose(applyMiddleware(...middlewares))(createStore);

export default store(reducers, {});
