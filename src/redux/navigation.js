import $ from 'jquery';

// uses Ducks pattern
const constants = {
  FETCH_DATA: 'NAVIGATION/FETCH_DATA',
  RECEIVE_DATA: 'ROYAL_FILE_UPLOADER/RECEIVE_DATA',
  RECEIVE_ERROR: 'ROYAL_FILE_UPLOADER/RECEIVE_ERROR'
};

export function fetchData(payload) {
  return dispatch => {
    dispatch({ type: constants.FETCH_DATA, payload });

    // in real life, here will be an api url
    const baseUrl =
      'https://raw.githubusercontent.com/independenc3/passion4coding/master/json';
    const method = 'GET';
    const files = ['categories', 'courses', 'verticals'];

    const asyncRequests = files.map(file => {
      const url = `${baseUrl}/${file}.json`;
      const ajax = $.ajax({ method, url });

      return Promise.resolve(ajax);
    });

    return Promise.all(asyncRequests)
      .then(([categories, courses, verticals]) => {
        dispatch({
          type: constants.RECEIVE_DATA,
          payload: { categories, courses, verticals }
        });
      })
      .catch(error => dispatch({ type: constants.RECEIVE_ERROR }));
  };
}

// export State to import in DefaultProps
export const defaultState = {
  loading: false,
  categories: [],
  courses: [],
  verticals: []
};

export default function(state = defaultState, action) {
  switch (action.type) {
    case constants.FETCH_DATA:
      return {
        ...state,
        loading: true,
        categories: [],
        courses: [],
        verticals: []
      };
    case constants.RECEIVE_DATA: {
      const { categories, courses, verticals } = action.payload;

      return {
        ...state,
        loading: false,
        categories: JSON.parse(categories),
        courses: JSON.parse(courses),
        verticals: JSON.parse(verticals)
      };
    }
    case constants.RECEIVE_ERROR:
      return {
        ...state,
        loading: false
      };
    default:
      return state;
  }
}
