export default function treeBuilder({ verticals, categories, courses }) {
  const categoriesTree = buildTree(categories, courses, 'Categories');

  const verticalsTree = buildTree(verticals, categoriesTree, 'Verticals');

  return verticalsTree;
}

function buildTree(parents, childs, filterKey) {
  const tree = parents.map(parent => {
    const branches = buildBranches(childs, filterKey, parent.Id);

    return {
      ...parent,
      childs: branches
    };
  });

  return tree;
}

function buildBranches(branches, filterKey, filterValue) {
  const filteredBranches = branches.filter(
    branch => branch[filterKey] === filterValue
  );

  return filteredBranches;
}
