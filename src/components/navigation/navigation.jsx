import React from 'react';
import { bindActionCreators } from 'redux';
import { Provider, connect } from 'react-redux';
import { BrowserRouter, Route } from 'react-router-dom';
import store from 'redux/store';
import * as Actions from 'redux/navigation';
import Container from './components/container.jsx';
import treeBuilder from './tree-builder';
import './navigation.css';

function mapStateToProps(state) {
  const tree = treeBuilder(state.navigation);

  return { ...state.navigation, tree };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

const ConnectedContainer = connect(mapStateToProps, mapDispatchToProps)(
  Container
);

export default function Navigation() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Route
          path="/:verticalId?/:categoryId?/:courseId?"
          component={ConnectedContainer}
        />
      </BrowserRouter>
    </Provider>
  );
}
