import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { NavLink } from 'react-router-dom';
import classNames from 'classnames';
import Tree from './tree.jsx';

export default class Branch extends Component {
  static propTypes = {
    branch: PropTypes.object.isRequired,
    currentPath: PropTypes.string.isRequired,
    treePath: PropTypes.string.isRequired
  };

  childs = () => {
    const { branch, currentPath } = this.props;

    if (!branch.childs || !this.isLinkActive()) {
      return null;
    }

    return (
      <Tree
        tree={branch.childs}
        currentPath={currentPath}
        treePath={this.branchPath()}
      />
    );
  };

  branchPath = () => {
    const { treePath, branch } = this.props;

    return `${treePath}/${branch.Id}`;
  };

  isLinkActive = () => this.props.currentPath.startsWith(this.branchPath());

  linkTo = () => {
    if (this.isLinkActive()) {
      return this.props.treePath;
    }

    return this.branchPath();
  };

  render() {
    const { branch } = this.props;

    const hasChilds = branch.childs && branch.childs.length > 0;

    const iconClassName = classNames({
      fa: true,
      'fa-angle-left': true,
      'pull-right': true,
      'navigation__branch-icon': true,
      'navigation__branch-icon--rotate': this.isLinkActive()
    });
    const linkClassName = classNames({
      'navigation__branch-link': true,
      'navigation__branch-link--active': this.isLinkActive()
    });
    const childClassName = classNames({
      navigation__branch__childs: true,
      'navigation__branch__childs--hide': !this.isLinkActive()
    });

    return (
      <li>
        <NavLink className={linkClassName} to={this.linkTo()}>
          <i className="fa fa-circle-o navigation__branch-icon" />

          {branch.Name}

          {hasChilds && <i className={iconClassName} />}
        </NavLink>

        <div className={childClassName}>{this.childs()}</div>
      </li>
    );
  }
}
