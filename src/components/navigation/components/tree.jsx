import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import Branch from './branch.jsx';

// treePath -> is location.path for current Tree Node
// currentPath -> is location.path for current App State
//
export default class Tree extends Component {
  static propTypes = {
    currentPath: PropTypes.string.isRequired,
    tree: PropTypes.array.isRequired,
    treePath: PropTypes.string
  };

  static defaultProps = {
    treePath: ''
  };

  branches = () => {
    const { tree, currentPath, treePath } = this.props;

    const branches = tree.map(branch => (
      <Branch
        key={branch.Id}
        branch={branch}
        currentPath={currentPath}
        treePath={treePath}
      />
    ));

    return branches;
  };

  render() {
    return <ul className="navigation__tree">{this.branches()}</ul>;
  }
}
