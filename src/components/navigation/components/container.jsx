import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import Tree from './tree.jsx';

export default class Container extends Component {
  static propTypes = {
    fetchData: PropTypes.func.isRequired,
    match: PropTypes.object.isRequired,
    tree: PropTypes.array.isRequired
  };

  componentDidMount() {
    this.props.fetchData();
  }

  render() {
    const { match, tree } = this.props;

    return (
      <div className="navigation">
        <Tree tree={tree} currentPath={match.url} />
      </div>
    );
  }
}
