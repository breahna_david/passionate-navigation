Passionate Navigation
============

Front End part of Coding Challenge [Passionate Navigation](https://github.com/independenc3/passion4coding)

Run
---

After you successfully pull this repo, you should download all the
required packages.

### `yarn` or `npm install`

then:

### `yarn start` or `npm start`

To Run the app in development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


Description
---

This application uses, redux store to fetch/manage back-end data.
In our case parsed json files from [here](https://github.com/independenc3/passion4coding/tree/master/json)

Once you chose, any of verticals, categories or courses you may notice that the url
has changed. That's due to react-router.

If you refresh the page (keep the same url) you'll get your navigation already folded.

Yay!
